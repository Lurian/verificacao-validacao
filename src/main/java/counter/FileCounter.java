package counter;

import choice.WordChoice;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class FileCounter {

    private File textFile;

    public FileCounter(String path) {
        this.textFile = new File(path);
    }

    public int count(WordChoice choice) {
        int count = 0;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(textFile));

            String line = reader.readLine();
            while (line != null) {
                String[] words = line.split("\\s");
                for (int i = 0; i < words.length; i++) {
                    String fileWord = choice.caseSensitive ? words[i] : words[i].toLowerCase();

                    if (choice.match(fileWord))
                        count++;
                }
                line = reader.readLine();
            }
            return count;
        } catch (IOException e) {
            System.out.println("Error while counting");
        }
        return 0;
    }
}
